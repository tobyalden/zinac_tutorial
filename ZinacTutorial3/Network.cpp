#include <stdio.h>
#include <stdarg.h>

#include "Network.h"
#include "ClockSyncCommon.h"

enum
{
    LOG_INFO,
    LOG_ERROR,
    LOG_DEBUG,
    LOG_TRACE
};

static const char* log_type_strings[] = {
    "INFO",
    "ERROR",
    "DEBUG",
    "TRACE"
};

// Basic logging function
// void Log(int type, const char* fmt, ...)
// {
//     va_list args;

//     va_start(args, fmt);

//     printf("[%s] ", log_type_strings[type]);
//     vprintf(fmt, args);
//     printf("\n");

//     va_end(args);
// }

#define DISABLED_LOG(...) 
#define NBN_LogInfo(...) Log(LOG_INFO,  __VA_ARGS__)
#define NBN_LogError(...) Log(LOG_ERROR, __VA_ARGS__)
#define NBN_LogDebug(...) Log(LOG_DEBUG, __VA_ARGS__)
#define NBN_LogTrace(...) DISABLED_LOG(__VA_ARGS__)

#include <winsock2.h>
#define NBNET_IMPL

#include "nbnet.h"
#include "net_drivers/udp.h"

#define HOST_PORT 50101
#define ECHO_PROTOCOL_NAME "clock_sync_test"
#define ECHO_SERVER_BUSY_CODE 42

static NBN_Connection *client = nullptr;

bool InitializeHost()
{
	if (NBN_GameServer_Start(ECHO_PROTOCOL_NAME, HOST_PORT, false) < 0)
	{
        Log(LOG_ERROR, "Failed to start the server.");
		return true;
	}
	return false;
}

void InitializeClient()
{
    // Start the client with a protocol name (must be the same than the one used by the server), the server ip address and port
    if (NBN_GameClient_Start(ECHO_PROTOCOL_NAME, "127.0.0.1", HOST_PORT, false, NULL) < 0)
    {
        Log(LOG_ERROR, "Failed to start client");

        // Error, quit the client application
        exit(-1);
    }
}

void HandleMessage()
{
    // Get info about the received message
    NBN_MessageInfo msg_info = NBN_GameServer_GetMessageInfo();

    assert(msg_info.sender == client);
    assert(msg_info.type == NBN_BYTE_ARRAY_MESSAGE_TYPE);

    // Retrieve the received message
    NBN_ByteArrayMessage* msg = (NBN_ByteArrayMessage*)msg_info.data;

    Log(LOG_INFO, "Received message! : %s", msg->bytes);

    // Destroy the received message
    NBN_ByteArrayMessage_Destroy(msg);
}

NetworkInputPackage ReadInputMessageCommon(const NBN_MessageInfo& msg_info)
{
    assert(msg_info.type == NBN_BYTE_ARRAY_MESSAGE_TYPE);

    // Retrieve the received message
    NBN_ByteArrayMessage* msg = (NBN_ByteArrayMessage*)msg_info.data;

    NetworkInputPackage InputPackage;
    memcpy(&InputPackage, msg->bytes, sizeof(NetworkInputPackage));

    // Log(LOG_INFO, "Input Recieved: Input = %d Frame = %d", InputPackage.InputCommand, InputPackage.FrameCount);

    // Destroy the received message
    NBN_ByteArrayMessage_Destroy(msg);

    return InputPackage;
}


NetworkInputPackage ReadInputMessageHost()
{
    // Get info about the received message
    NBN_MessageInfo msg_info = NBN_GameServer_GetMessageInfo();

    return ReadInputMessageCommon(msg_info);
}

NetworkInputPackage ReadInputMessageClient()
{
    // Get info about the received message
    NBN_MessageInfo msg_info = NBN_GameClient_GetMessageInfo();

    return ReadInputMessageCommon(msg_info);
}

void SendInputMessageHost(NetworkInputPackage InputPackage)
{
    // Create a nbnet outgoing message
    NBN_OutgoingMessage* outgoing_msg = NBN_GameServer_CreateByteArrayMessage((uint8_t*)&InputPackage, sizeof(InputPackage));

    assert(outgoing_msg);

    // Unreliably send it to the server
    if (NBN_GameServer_SendUnreliableMessageTo(client, outgoing_msg) < 0)
    {
        Log(LOG_INFO, "Error: Failed to send outgoing message!");
        exit(-1);
    }
}

void SendInputMessageClient(NetworkInputPackage InputPackage)
{
    // Create a nbnet outgoing message
    NBN_OutgoingMessage* outgoing_msg = NBN_GameClient_CreateByteArrayMessage((uint8_t*)&InputPackage, sizeof(InputPackage));

    assert(outgoing_msg);

    // Reliably send it to the server
    if (NBN_GameClient_SendUnreliableMessage(outgoing_msg) < 0)
    {
        Log(LOG_INFO, "Error: Failed to send outgoing message!");
        exit(-1);
    }
}

NetworkInputPackage TickNetworkHost(NetworkInputPackage InputPackage, bool& bRecievedInput, bool& bIsConnected)
{
    bRecievedInput = false;

    if(client)
    {
        for (int i = 0; i < 10; i++)
        {
            SendInputMessageHost(InputPackage);
        }
    }
    
    NetworkInputPackage ClientInputPackage = { 0, 0 };

    // Update the server clock
    NBN_GameServer_AddTime(1.0 / 60.0);

    int ev;

    // Poll for server events
    while ((ev = NBN_GameServer_Poll()) != NBN_NO_EVENT)
    {
        if (ev < 0)
        {
            Log(LOG_ERROR, "Something went wrong");

            // Error, quit the server application
            exit(-1);
        }

        switch (ev)
        {
            // New connection request...
            case NBN_NEW_CONNECTION:
                // Echo server work with one single client at a time
                if (client == nullptr)
                {
                    client = NBN_GameServer_GetIncomingConnection();

                    NBN_GameServer_AcceptIncomingConnection();
                    Log(LOG_INFO, "Accepting client connection!");
                    bIsConnected = true;
                }

                break;

                // The client has disconnected
            //case NBN_CLIENT_DISCONNECTED:
            //    assert(NBN_GameServer_GetDisconnectedClient()->id == client->id);

            //    client = NULL;
            //    break;

            // A message has been received from the client
            case NBN_CLIENT_MESSAGE_RECEIVED:
                ClientInputPackage = ReadInputMessageHost();
                bRecievedInput = true;
                break;
        }
    }

    // Pack all enqueued messages as packets and send them
    if (NBN_GameServer_SendPackets() < 0)
    {
        Log(LOG_ERROR, "Failed to send packets");

        // Error, quit the server application
        exit(-1);
    }

    return ClientInputPackage;
}

NetworkInputPackage TickNetworkClient(NetworkInputPackage InputPackage, bool& bRecievedInput, bool& bIsConnected)
{
    bRecievedInput = false;

    NetworkInputPackage HostInputPackage = { 0, 0 };

    for (int i = 0; i < 10; i++)
    {
        SendInputMessageClient(InputPackage);
    }

    // Update client clock
    NBN_GameClient_AddTime(1.0/60.0);

    int ev;

    // Poll for client events
    while ((ev = NBN_GameClient_Poll()) != NBN_NO_EVENT)
    {
        if (ev < 0)
        {
            Log(LOG_ERROR, "An error occured while polling client events. Exit");

            // Stop main loop
            exit(-1);
        }

        switch (ev)
        {
            // Client is connected to the server
            case NBN_CONNECTED:
                //OnConnected();
                Log(LOG_INFO, "Connected to host!");
                bIsConnected = true;
                break;

                // Client has disconnected from the server
            //case NBN_DISCONNECTED:
            //    //OnDisconnected();
            //    Log(LOG_ERROR, "Disconnected from host!");
            //    break;

            // A message has been received from the server
            case NBN_MESSAGE_RECEIVED:
                HostInputPackage = ReadInputMessageClient();
                bRecievedInput = true;
                break;
        }
    }

    // Pack all enqueued messages as packets and send them
    if (NBN_GameClient_SendPackets() < 0)
    {
        Log(LOG_ERROR, "Failed to send packets");

        // Error, quit the server application
        exit(-1);
    }

    return HostInputPackage;
}