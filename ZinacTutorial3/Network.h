#pragma once

constexpr unsigned int NET_PACKET_INPUT_HISTORY_SIZE = 10;

struct NetworkInputPackage
{
	unsigned int InputHistory[NET_PACKET_INPUT_HISTORY_SIZE];
	int FrameCount{ 0 };
	int FrameDelta{ 0 };
};

void Log(int type, const char* fmt, ...);

bool InitializeHost();
void InitializeClient();

NetworkInputPackage TickNetworkHost(NetworkInputPackage InputCommand, bool& bRecievedInput, bool& bIsConnected);
NetworkInputPackage TickNetworkClient(NetworkInputPackage InputCommand, bool& bRecievedInput, bool& bIsConnected);