#include "raylib.h"
#include "Network.h"
#include <climits>
#include <vcruntime_string.h>
#include "ClockSyncCommon.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

constexpr int MAX_ROLLBACK_FRAMES = 10;

enum class NetworkState
{
	None,
	Host,
	Client
};

enum class NetworkSyncType
{
	LockStep,
	Rollback
};

enum class BootNetworkSetting
{
	NoConnection,
	Host,
	Client
};

enum class InputCommand : unsigned int
{
	None = 0,
	Up = 1,
	Down = 2,
	Left = 4,
	Right = 8,
};

struct Vector2i
{
	int X { 0 };
	int Y { 0 };
};

struct EntityState
{
	Vector2i Position;
	Vector2i Velocity;
};

struct SimulationState
{
	EntityState Entities[2];
	unsigned int Inputs[2];
	int FrameCount{ 0 };
};

void UpdateEntity(unsigned int Input, EntityState& Entity)
{
	Entity.Velocity.X = 0;
	Entity.Velocity.Y = 0;

	if (Input & static_cast<unsigned int>(InputCommand::Left))
	{
		Entity.Velocity.X = -10;
	}
	if (Input & static_cast<unsigned int>(InputCommand::Right))
	{
		Entity.Velocity.X = 10;
	}
	if (Input & static_cast<unsigned int>(InputCommand::Up))
	{
		Entity.Velocity.Y = 10;
	}
	if (Input & static_cast<unsigned int>(InputCommand::Down))
	{
		Entity.Velocity.Y = -10;
	}

	Entity.Position.X += Entity.Velocity.X;
	Entity.Position.Y += Entity.Velocity.Y;
}

void DrawEntity(const EntityState& State, Color color)
{
	DrawCircle(State.Position.X + SCREEN_WIDTH / 2, SCREEN_HEIGHT - State.Position.Y, 50, color);
}

void UpdateSimulation(SimulationState& GameState)
{
	UpdateEntity(GameState.Inputs[0], GameState.Entities[0]);
	UpdateEntity(GameState.Inputs[1], GameState.Entities[1]);
}

int main(int argc, char** argv)
{
	const char* WindowTitle = "Clock Sync Test";

	BootNetworkSetting NetworkModeSetting = BootNetworkSetting::NoConnection;

	NetworkSyncType NetSyncType = NetworkSyncType::Rollback;

	if (argc >= 2)
	{
		if (argv[1][0] == 'c')
		{
			Log(LOG_INFO, "Started client from command line arguments");
			NetworkModeSetting = BootNetworkSetting::Client;
		}
		else if (argv[1][0] == 's')
		{
			Log(LOG_INFO, "Started host from command line arguments");
			NetworkModeSetting = BootNetworkSetting::Host;
		}

		if (argc >= 3)
		{
			// Override rollback mode and run the network matches in deterministic lock step mode (delay based)
			if (argv[2][0] == 'd')
			{
				NetSyncType = NetworkSyncType::LockStep;
			}
		}
	}


	NetworkState NetState = NetworkState::None;

	int LocalSide = 0;
	int OpponentSide = 1;

	// Latest frame counter recieved from the other game client
	int NetworkFrameCount = 0;

	if (NetworkModeSetting == BootNetworkSetting::Host)
	{
		InitializeHost();
		Log(LOG_INFO, "Finished calling initialize host");
		NetState = NetworkState::Host;
		LocalSide = 0;
		OpponentSide = 1;
		WindowTitle = "Clock Sync Test P1 : Host";
	}
	else if (NetworkModeSetting == BootNetworkSetting::Client)
	{
		InitializeClient();
		Log(LOG_INFO, "Finished calling initialize client");
		NetState = NetworkState::Client;
		LocalSide = 1;
		OpponentSide = 0;
		WindowTitle = "Clock Sync Test P2 : Client";
	}

	InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, WindowTitle);

	switch (NetworkModeSetting)
	{
	case BootNetworkSetting::Host:
		SetWindowPosition(200, 50);
		break;
	case BootNetworkSetting::Client:
		SetWindowPosition(1200, 50);
		break;
	}

	SetTargetFPS(60);

	SimulationState GameState;

	//stored state for rolling back
	SimulationState SavedGameState;

	GameState.Entities[0].Position.X = -200;
	GameState.Entities[0].Position.Y = 100;
	GameState.Entities[1].Position.X = 200;
	GameState.Entities[1].Position.Y = 100;

	GameState.Inputs[0] = static_cast<unsigned int>(InputCommand::None);
	GameState.Inputs[1] = static_cast<unsigned int>(InputCommand::None);

	// Network Input Delay
	constexpr int NET_INPUT_DELAY = 1;

	// The input value we pull on the local client.
	unsigned int PolledInput = 0;

	// Recieve input history
	int InputHistoryIndex = 0;


	// Indicates the most recent confirmed frame from the other player recieved over the network
	int LatestNetworkFrame = -1;

	constexpr int INPUT_HISTORY_SIZE = 3 * 60 * 60;

	// Records the input history for every frame for both player 1 and player 2
	unsigned int InputHistory[2][INPUT_HISTORY_SIZE];

	memset(InputHistory, 0, 2*INPUT_HISTORY_SIZE*sizeof(unsigned int));

	bool bIsConnected = false;

	// indicates that we should record the input (only once per update)
	bool bUpdatePolledInput = true;

	// Before we start the game store the initial game state
	memcpy(&SavedGameState, &GameState, sizeof(SavedGameState));

	// Number of out-of-sync frames.
	// int OutOfSyncFrames = 0;

	// Record the frame number of the last game state frame that we saved.
	int LastSavedGameStateFrame = -1;

	// Main game loop
	while (!WindowShouldClose())
	{

		if (NetworkModeSetting == BootNetworkSetting::NoConnection)
		{
			// Save game state
			if (IsKeyPressed(KEY_F1))
			{
				memcpy(&SavedGameState, &GameState, sizeof(SavedGameState));
			}
			// Restore game state
			else if (IsKeyPressed(KEY_F3))
			{
				memcpy(&GameState, &SavedGameState, sizeof(GameState));
			}
		}
		// Reset buffered input each frame
		PolledInput = 0;

		if (bUpdatePolledInput)
		{
			if (IsWindowFocused())
			{
				// Update Input
				if (IsKeyDown(KEY_UP))
				{
					PolledInput |= static_cast<unsigned int>(InputCommand::Up);
				}
				if (IsKeyDown(KEY_DOWN))
				{
					PolledInput |= static_cast<unsigned int>(InputCommand::Down);
				}
				if (IsKeyDown(KEY_LEFT))
				{
					PolledInput |= static_cast<unsigned int>(InputCommand::Left);
				}
				if (IsKeyDown(KEY_RIGHT))
				{
					PolledInput |= static_cast<unsigned int>(InputCommand::Right);
				}
			}

			// Record this input in the history buffer for sending to the other player
			if (GameState.FrameCount < INPUT_HISTORY_SIZE - NET_INPUT_DELAY)
			{
				InputHistory[LocalSide][GameState.FrameCount + NET_INPUT_DELAY] = PolledInput;
			}

			bUpdatePolledInput = false;

		}



		NetworkInputPackage LatestInputPackage = { 0, -1 };

		bool bRecievedNetworkInput = false;

		NetworkInputPackage ToSendNetPackage;

		int LatestLocalFrameDelta = GameState.FrameCount - LatestNetworkFrame;

		// Prepare the network package to send to the opponent
		{
			// Log(LOG_INFO, "Sending Net Input: Frame[%d]", GameState.FrameCount + NET_INPUT_DELAY);

			const int InputStartIndex = GameState.FrameCount - NET_PACKET_INPUT_HISTORY_SIZE + 1 + NET_INPUT_DELAY;
			// Fill the network package's input history with out local input
			for (int i = 0; i < NET_PACKET_INPUT_HISTORY_SIZE; i++)
			{
				const int InputIndex = InputStartIndex + i;
				if (InputIndex >= 0)
				{
					ToSendNetPackage.InputHistory[i] = InputHistory[LocalSide][InputIndex];
				}
				else
				{
					// Sentinal value
					ToSendNetPackage.InputHistory[i] = UINT_MAX;
				}

				// Log(LOG_INFO, "Input [%u] Frame[%d]", ToSendNetPackage.InputHistory[i], GameState.FrameCount + NET_INPUT_DELAY + i);
			}

			// Indicates the frame the final input in the buffer is designated for
			ToSendNetPackage.FrameCount = GameState.FrameCount + NET_INPUT_DELAY;

			// Send frame delta (time delta) to the other player.
			ToSendNetPackage.FrameDelta = LatestLocalFrameDelta;
		}


		// Update network
		if (NetState == NetworkState::Host)
		{
			LatestInputPackage = TickNetworkHost(ToSendNetPackage, bRecievedNetworkInput, bIsConnected);
		}
		else if (NetState == NetworkState::Client)
		{
			LatestInputPackage = TickNetworkClient(ToSendNetPackage, bRecievedNetworkInput, bIsConnected);
		}

		if (bRecievedNetworkInput)
		{
			//Log(LOG_INFO, "Recieved Net Input: Frame[%d]", LatestInputPackage.FrameCount);

			const int StartFrame = LatestInputPackage.FrameCount - NET_PACKET_INPUT_HISTORY_SIZE + 1;
			for (int i = 0; i < NET_PACKET_INPUT_HISTORY_SIZE; i++)
			{
				const int CheckFrame = StartFrame + i;
				if (CheckFrame == (LatestNetworkFrame + 1))
				{
					// Advance the network frame so we know that we have input for the simulation
					LatestNetworkFrame++;
				}

				//Log(LOG_INFO, "Input [%u] Frame[%d]", LatestInputPackage.InputHistory[i], StartFrame + i);

				// Record the other player's input to be used in the game simulation
				InputHistory[OpponentSide][StartFrame + i] = LatestInputPackage.InputHistory[i];
			}
		}

		// Indicates that we perform one update of the game simulation
		bool bUpdateNextFrame = (GameState.FrameCount == 0);
		 
		// The number of times we need to run the game simulation.
		int SimulatedFrames = 1;

		if (NetSyncType == NetworkSyncType::Rollback)
		{
			// Limit how far ahead we can get ahead of the opponent.
			bUpdateNextFrame = GameState.FrameCount < (LatestNetworkFrame + MAX_ROLLBACK_FRAMES - 1);

			// Check the frame delta difference of the local client and the remote in order to sync clocks.
			if (LatestLocalFrameDelta - LatestInputPackage.FrameDelta > 1)
			{
				bUpdateNextFrame = false;
			}
		}
		else if (NetSyncType == NetworkSyncType::LockStep)
		{
			// Only update the game simulation when we have input for the target frame
			if (LatestNetworkFrame >= GameState.FrameCount)
			{
				bUpdateNextFrame = true;
			}
		}


		// TODO: If we get through the series and Zinac never adds his own solution to the race condition that bIsConnected solves
		// (namely, ticking the game before a connection has actually been established; NetworkState::None only indicates that
		// a client or host has not yet INITIATED a connection, not succeeded in actually connecting), it might be worth asking him on Twitter about it!
		if (NetState == NetworkState::None || !bIsConnected)
		{
			bUpdateNextFrame = false;
		}

		// Allow game simulation when not connected to a client
		if (NetworkModeSetting == BootNetworkSetting::NoConnection)
		{
			bUpdateNextFrame = true;
		}
		// Indicates whether or not we are resimulating the game after a rollback restore event.
		bool bIsResimulating = false;

		// How many times we run the game simulation.
		int SimulationFrames = 1;

		// Detect if we have new inputs so we can resimulate the game (rollback).
		if (LatestNetworkFrame > LastSavedGameStateFrame)
		{
			bIsResimulating = true;
			bUpdateNextFrame = true;

			// Calculates the number of frames we need to resimulate plus the current frame.
			SimulationFrames = GameState.FrameCount - LastSavedGameStateFrame;
		
			Log(LOG_INFO, "Roll: SimulationFrames[%d] LatestNetworkFrame[%d] LastSavedGameStateFrame[%d] GameState.FrameCount[%d]", SimulationFrames, LatestNetworkFrame, LastSavedGameStateFrame, GameState.FrameCount);

			// Restore the last saved game state.
			memcpy(&GameState, &SavedGameState, sizeof(GameState));
		}

		// Force the game simulation to pause while the button is held.
		if (IsKeyDown(KEY_PAUSE))
		{
			bUpdateNextFrame = false;
		}

		if (bUpdateNextFrame)
		{
			// Run the game simulation the number of times previously determined
			for (int SimFrame = 0; SimFrame < SimulationFrames; SimFrame++)
			{

				//Log(LOG_INFO, "Game ticked [%d]", GameState.FrameCount);

				// Assign the local player's input with input delay applied
				GameState.Inputs[LocalSide] = InputHistory[LocalSide][GameState.FrameCount];

				// Assign our opponent's input from the network
				GameState.Inputs[OpponentSide] = InputHistory[OpponentSide][GameState.FrameCount];

				SyncLog("Frame(%d) p1[%x] p2[%x]", GameState.FrameCount, GameState.Inputs[0], GameState.Inputs[1]);

				UpdateSimulation(GameState);

				bUpdatePolledInput = true;
				GameState.FrameCount++;

				if (bIsResimulating)
				{
					// Save game state when we have input for this frame.
					if (LatestNetworkFrame >= (GameState.FrameCount - 1))
					{
						LastSavedGameStateFrame = GameState.FrameCount - 1;
						memcpy(&SavedGameState, &GameState, sizeof(SavedGameState));
					}
				}

				// If we are ahead of the latest confirmed input frame track the number of forward sim frames.
				// if (GameState.FrameCount > LatestNetworkFrame)
				// {
				// 	OutOfSyncFrames++;
				// }
			}
		}

		// Drawing
		BeginDrawing();
		ClearBackground(DARKGRAY);
		DrawEntity(GameState.Entities[0], RED);
		DrawEntity(GameState.Entities[1], BLUE);

		DrawText(TextFormat("P1: [%i, %i]", GameState.Entities[0].Position.X, GameState.Entities[0].Position.Y), 10, 40, 10, GREEN);
		DrawText(TextFormat("P2: [%i, %i]", GameState.Entities[1].Position.X, GameState.Entities[1].Position.Y), 10, 60, 10, GREEN);
		DrawText(TextFormat("Frame Count [%d]", GameState.FrameCount), 10, 80, 10, WHITE);
		DrawText(TextFormat("Net Frame [%d]", LatestInputPackage.FrameCount), 10, 100, 10, WHITE);
		DrawText(TextFormat("Input Delay [%d]", NET_INPUT_DELAY), 10, 120, 10, WHITE);
		DrawText(TextFormat("Local Frame Delta [%d]", LatestLocalFrameDelta), 10, 140, 10, WHITE);

		EndDrawing();
	}

	CloseWindow();

	return 0;
}

